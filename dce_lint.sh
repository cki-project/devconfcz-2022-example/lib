#!/bin/bash
set -euo pipefail

[[ "$(type -P python3)" != /usr* ]] || export PIP_USER=true
dce_pip_install.sh coverage flake8 "isort[colors]" mypy pydocstyle pylint pytest

FAILED=()
flake8 --max-line-length 100 --exclude '.*' || FAILED+=(flake8)
pydocstyle --match='.*\.py' || FAILED+=(pydocstyle)
isort --color --force-single-line-imports --force-sort-within-sections --line-length 100 --skip-glob '.*' --check --diff  . || FAILED+=(isort)
mypy --strict -m "$@" || FAILED+=(mypy)
pylint --rcfile setup.cfg --max-line-length 100 "$@" tests || FAILED+=(pylint)
coverage run --source "$(IFS=,; echo "$*")" --branch -m pytest --junitxml=coverage/junit.xml --color=yes -v -r s || FAILED+=(pytest)

coverage report -m || true
coverage html -d coverage/ || true
coverage xml -o coverage/coverage.xml || true

if [ "${#FAILED[@]}" -gt 0 ]; then
    echo "Failed linting steps: ${FAILED[*]}"
    exit 1
fi
